import java.util.Scanner;

public class Array5 {
    public static void main(String[] args) {
        int sum = 0;
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0 ; i< 3 ; i++){
            System.out.print("Please input arr[" + i + "] :");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0 ; i< 3 ; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        for (int i = 0 ; i< 3 ; i++){
            sum = sum + arr[i];
        }
        System.out.println("sum = " + sum);
        System.out.println();
    }
}
