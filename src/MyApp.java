import java.util.Scanner;

public class MyApp {
    //ส่วนที่1
    static void printWelcome(){
        System.out.println("Welcome to my app!!!");
    }



    //ส่วนที่2
    static void printMenu(){
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }



    //ส่วนที่3
    static int inputChoice(){
        int choice;
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input your choice(1-3) :");
            choice = sc.nextInt();
            if (choice>=1 && choice<=3){
                return choice;
            } else {
            System.out.println("Error: Please input between 1-3");
            }
        }
    }



    //ส่วนที่4
    static int inputTime(){
        int time;
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input time :");
            time = sc.nextInt();
            if (time>=1){
                return time;
            } else {
            System.out.println("Error: Please input positive number!!!");
            }
        }
    }
    static void printHelloworldNTime(){
        int time = inputTime();
        printHello(time);
    }
    static void exitProgram(){
        System.out.println("Bye!!!");
        System.exit(0);
    }
    static void printHello(int time){
        for(int i=0 ; i<time ; i++){
            System.out.println("Hello World!!!");
        }
    }
    static void addTwoNumbers(){
        int first = inputFirst();
        int second = inputSecond();
        int result = add(first,second);
        printResult(result);
    }
    static int add(int first,int second){
        int result = first + second;
        return result;
    }
    static void printResult(int result){
        System.out.println("Result = " + result);
    }
    static int inputFirst(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number :");
        int frist = sc.nextInt();
        return frist;  
    }
    static int inputSecond(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number :");
        int second = sc.nextInt();
        return second;
    }

    public static void main(String[] args) {
        int choice = 0;
        while(true){
        printWelcome();
        printMenu(); 
        choice = inputChoice();
            switch(choice) {
                case 1:
                    //Hello world
                    printHelloworldNTime();
                    break;
                case 2:
                    //add
                    addTwoNumbers();
                    break;
                case 3:
                    //exit
                    exitProgram();
                    break;
            }
        }
    }
}
