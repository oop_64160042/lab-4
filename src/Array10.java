import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        int num = 0;
        int arr[];
        int uniqeArr[];
        int uniSize = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        num = sc.nextInt();
        arr = new int[num];
        uniqeArr = new int[num];
        for (int i = 0 ; i<arr.length ; i++) {
            System.out.print("Element " + i + ": ");
            arr[i] = sc.nextInt();
            int index = -1;
            for (int j = 0; j<uniSize ; j++) {
                if(uniqeArr[j] ==arr[i]) {
                    index = j;
                }
            }
            if(index<0){
                uniqeArr[uniSize] = arr[i];
                uniSize++;

            }
        }
        System.out.print("All number = ");
        for (int i = 0 ; i< uniSize ; i++) {
            System.out.print(uniqeArr[i] + " ");
        }
        System.out.println();
    }
}
